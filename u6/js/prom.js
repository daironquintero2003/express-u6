const data = [
    {
        product: "strawberry",
        prize: 2000,

    },
    {
        product: "potato",
        prize: 2500,
    }
];

function getData(){
    //promise =  es una promesa usada para que envie los datos despues de un tiempo
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{resolve(data)},3500);
    });
}

function getRamdom(){
    return Math.random()*10;
}
getData().then((info)=>console.log(info));
console.log(getRamdom());