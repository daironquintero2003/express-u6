let a = `
{
    "squadName": "Super hero squad",
    "homeTown": "Metro City",
    "formed": 2016,
    "secretBase": "Super tower",
    "active": true,
    "members": [{
            "name": "Molecule Man",
            "age": 29,
            "secretIdentity": "Dan Jukes",
            "powers": ["Radiation resistance", "Turning tiny", "Radiation blast"]
        },
        {
            "name": "Madame Uppercut",
            "age": 39,
            "secretIdentity": "Jane Wilson",
            "powers": [ "Million tonne punch", "Damage resistance", "Superhuman reflexes"]
        },
        {
            "name": "Eternal Flame",
            "age": 1000000,
            "secretIdentity": "Unknown",
            "powers": [  "Immortality",  "Heat Immunity", "Inferno",  "Teleportation",  "Interdimensional travel"]
        }
    ]
}
`;
const object = {
    name: "Carlos",
    age: 38,
    active: true,
    points: [3, 5, 2, 4, 6, 1],
    address: {
        dir: "Fake street 123",
        phone: "32312212"
    },
    children: [{
        name: "Gabo",
        grade: 2
    }, {
        name: "Maleja",
        grade: -1
    }]
};

console.log( "Value ", object );
